module STree
 (STree,
   nil, -- STree a
   isNil, -- STree a -> Bool
   isNode, -- STree a -> Bool
   left, -- STree a -> STree a
   right, -- STree a -> STree a
   val, -- STree a -> STree a
   size, -- STree a -> Int
   isOrdered, -- Ord a => STree a -> Bool
   isOrdered, -- Ord a => [a] -> Bool
   inOrder, -- STree a -> [a]
   indexTree, -- Int -> Tree a -> Maybe a
   insTree, -- Ord a => a -> STree a -> STree a
   delTree, -- Ord a => a -> STree a -> STree a
   minVal, -- Ord a => a -> STree a -> Maybe a
   maxVal, -- Ord a => a -> STree a -> Maybe a
   bSort, -- Ord a => [a] -> [a]
   successor, -- Ord a => a -> STree a -> Maybe a
   closest, -- Ord a => a -> STree a -> Maybe a
   between -- Ord a => a -> a -> STree a -> [a]
  ) where

data STree a = Nil | Node a (STree a) (STree a) deriving (Show,Eq)


-- Example of Maybe and Just
myHead :: [a] -> Maybe a
myHead [] = Nothing
myHead (x:xs) = Just x

myHead2 :: [a] -> a
myHead2 (x:xs) = x
myHead2 [] = error "No Head" -- Exceptional case

-- Exercise 1
nil :: STree a
nil = Nil

-- Exercise 2
isNil :: (Ord a) => STree a -> Bool
isNil Nil = True
isNil  _  = False

-- Exercise 3
isNode :: STree a -> Bool
isNode Nil = False
isNode _ = True

-- Exercise 4
left :: STree a -> STree a
left Nil = error "Empty Tree"
left (Node _ ln rn) = ln

right :: STree a -> STree a
right Nil = error "Empty Tree"
right (Node _ ln rn) = rn

-- Exercise 5
val :: STree a -> STree a
val Nil = error "No value for empty tree"
val x = x

-- Exercise 6
size :: STree a -> Int
size Nil = 0
size (Node _ ln rn) = 1 + size ln + size rn

-- Exercise 7
isOrdered :: (Ord a) => STree a -> Bool
isOrdered Nil = True
isOrdered (Node x ln rn) = isOrdered . inorder

-- Exercise 8
isOrdered :: Ord a => STree a -> Bool
isOrdered Nil = True
isOrdered (Node tl x tr) = STree tl && STree tr && (isEmpty tl || maxt tl < x) && (isEmpty tr || x < mint tr)

-- Exercise 9
isOrdered :: Ord a => [a] -> Bool
isOrdered [] = True
isOrdered [x] = True
isOrdered (x:y:xs) = x <= y && isOrdered (y:xs)

-- Exercise 10
inorder :: (Ord a) => STree a -> [a]
inorder Nil = []
inorder (Node t1 v t2) = inorder t1 ++ [v] ++ inorder t2

-- Exercise 11

insTree :: Ord a => a -> STree a -> STree a
insTree r Nil = Node Nil v Nil
insTree r (Node x tl tr)
| x == r = Node x tl tr
| r < x = Node x (insTree r tl) tr
| otherwise = Node x tl (insTree r tr)	

-- Exercise 12

deletemax :: Ord a => STree a -> (a, STree a)
-- At the rightmost node
deletemax (Node x tl Nil) = (x, tl)
-- Always descend right
deletemax (Node x tl tr) = (y, Node x tl ty)
 where (y, ty) = deletemax tr

delete :: Ord a => a -> STree a -> STree a
delete r Nil = Nil
delete (Node x tl tr) r
 | r < x = Node x (delete tl r) tr
 | r > x = Node r tl (delete tr v)
 | otherwise = if (tl == Nil) then tr
 else (Node y ty tr)
 where (y, ty) = deletemax tl
 
-- Exercise 13
minVal :: Ord a => STree a -> a
-- Assume that the input tree is non-Nil
minVal (Node t1 x t2) = min x (min y z)
 where
 y = if (t1 == Nil) then x
 else min t1
 z = if (t2 == Nil) then x
 else min t2

maxVal :: Ord a => STree a -> a
-- Assume that the input tree is non-Nil
maxVal (Node t1 x t2) = max x (max y z)
 where
 y = if (t1 == Nil) then x
 else maxVal t1
 z = if (t2 == Nil) then x
 else maxVal t2

-- Exercise 14
makeTree :: Ord a => [a] -> STree a
makeTree = foldl insTree Nil

bSort :: Ord a => [a] -> [a]
bSort = inorder . makeTree

-- Exercise 15
successor :: Ord a => a -> STree a -> Maybe a
successor v t 
  | isNil t = Nothing
  | v >= (fromMaybe (error "no successor") (maxVal t)) = Nothing
  | otherwise = Just (head (filter (>v) (inOrder t)))

-- Exercise 16

-- Exercise 17
between :: Ord a => a -> a -> STree a -> [a]
between v1 v2 t
  | isNil t   = []
  | otherwise = filter (>v1) (filter (<v2) (inOrder t))






