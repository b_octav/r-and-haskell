-- Lab3 : Emese Bodrojan
-- Due date 7th of November

module Lab3 where

-------------
-- Exercise 5
-- a)Define the list primes of all prime numbers with the sieve of Erathostenes.
-- x will refer to the first element in the list and xs (pronounced exes, as in the plural of x) refers to the remainder of the list.
-- primes is recursive. The list that it generates consists of the first element of the list passed into it and the output of primes when passed the remainder of the list after some filtering.
-- filter function expects two arguments. The first argument is a predicate and the second is a list. 
-- filter returns a new list consisting of the elements from the initial list that yield True when passed to the predicate.
-- The . operator composes two functions i.e. (f . g) x == f (g x).
primes (x:xs) = x : primes (filter ((/= 0) . (`mod` x)) xs)

-- b)Use primes to define the function prime n which returns the n-th prime number.
-- primes [2, 3, 4, 5, 6...] = 2 : primes [3, 5, 7, 9, 11...]            -> 1st prime number
--                           = 2 : 3 : primes [5, 7, 11, 13, 17...]      -> 2nd prime number
--                           = 2 : 3 : 5 : primes [7, 11, 13, 17, 19...] -> 3rd prime number
--                           = 2 : 3 : 5 : 7 : primes [11, 13, 17, 19, 23...] 
--                           = 2 : 3 : 5 : 7 : 11 : primes [13, 17, 19, 23, 29...]
-- prime function has to be supplied with a list of the positive integers starting at 2
-- !! (n-1) returns the element of the stream xs at index n-1,this is because the head of the stream has index 0.
prime n = primes [2..] !! (n-1)
